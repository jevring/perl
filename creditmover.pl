use strict;
use diagnostics;

#!/usr/bin/perl

# credit mover:
# reads credits from files in a dir, fins the matching files in another dir, and gets the credits from the matching file 
# and puts them in the original file

#open the dir where the current userfiles are
my $current = "/glftpd/ftp-data/users";
my $old = "/glftpd/oldusers";
my $f;
opendir (DIR, $current) || die "could not open $current\n";
	while (defined($f = readdir DIR)) {
		#check if the entry is a file (-f)
		if (-f "$current/$f") {
			#check that the corresponding file exists
			if (-f "$old/$f") {
				print "doing $f\n";
				transferCredits("$current/$f", "$old/$f");
			} else {
				print "userfile $f couldn't be found in the old userfiles, must be new user\n";	
			}
		}
	}
closedir DIR;

sub transferCredits {
	my $currentFile = shift;
	my $oldFile	= shift;
	my $line;
	my ($ccredits, $ocredits, $newcredits);
	open (CURRENT, $currentFile) || die "could not open $currentFile\n";
	open (OLD, $oldFile) || die "could not open $oldFile\n";
	#cache file contents
	my @oldFileCache = <OLD>;
	my @currentFileCache = <CURRENT>;
	close CURRENT;
	close OLD;
	
	#go through the current file
	foreach $line (@oldFileCache) {
		if ($line =~ /CREDITS (\d{1,})/) {
			$ocredits = $1;
			$line =~ s/CREDITS \d{1,}/CREDITS 0/;
		}
	}
	open (WRITE, ">$oldFile") || die "could not open $oldFile for writing\n";
	print WRITE @oldFileCache;
	close WRITE;
	
	foreach $line (@currentFileCache) {
		if ($line =~ /CREDITS (\d{1,})/) {
			$ccredits = $1;
			$newcredits = $ccredits + $ocredits;
			$line =~ s/CREDITS \d{1,}/CREDITS $newcredits/;
		}
	}
	open (WRITE, ">$currentFile") || die "could not open $currentFile for writing\n";
	print WRITE @currentFileCache;
	close WRITE;
		
	close OLD;
	close CURRENT;
}

