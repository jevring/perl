#!c:\Perl\bin\perl

# this script will check dirs and subdirs and compile stats on groups
# in the various subdirs

use Math::BigInt;
use Getopt::Long;
my @good_dirs = ("d:\\Music\\!!Singles", "d:\\Music");
my @bad_dirs = ("d:\\Music\\!deleted", "d:\\!deleted-old");
my $sort_by; #default is strange but quite nice, kind of a mixed sort

GetOptions ('good:s' => \@good_dirs, 'bad:s' => \@bad_dirs, 'sort:s' => \$sort_by);

# flags for: bad dirs, good dirs, sort order, display thresholds, show groups or artists

# Usage: perl mp3_group_dir_checker.pl -g !!Singles -g !checked -g !Livesets -b !deleted [-s good|bad|none(default)]
# dirs need to be in the current directoy, or have a fully qualified path
# will display percentage in the respective category, i.e. percent kep out of all the kept and so on

my %releases;
my %dirs;
my $good_total = 0;
my $bad_total = 0;
my $get_deleted = 0;
# Try to reuse this loop for all the dirs...
foreach my $good_dir (@good_dirs) {
	print "doing GOOD: $good_dir\n";
	opendir (GOOD, $good_dir) || die "Could not open $good_dir\n$!";
	while (defined($d = readdir GOOD)) {
		$good_total++;
		#check if the entry is a dir (-d)
		if (-d "$good_dir/$d") {
			$d =~ /.*-(.+)$/g;
			#found a release, increase it's frequency
			my $group = $1;
			#check that the groupname doesn't contain spaces (if it does, it's not a groupname)
			if(length($group) > 10 || length($group) == 0 || $group =~ / /) {
				print $d . "\n";
				$releases{'NOGROUP'}{'good'}++;
				$dirs{$good_dir}{'good'}++;
			}
			unless ($group =~ / /) {
				#for some reason, this can't be put in the line above
				if (length($group) < 10) {
					$group = uc($group);
					$releases{$group}{'good'}++;
					$dirs{$good_dir}{'good'}++;
				}
			}
		}
	}
	closedir GOOD;
}

foreach my $bad_dir (@bad_dirs) {
	print "doing BAD: $bad_dir\n";
	opendir (BAD, $bad_dir) || die "Could not open $bad_dir\n$!";
	while (defined($d = readdir BAD)) {
		$bad_total++;
		#check if the entry is a dir (-d)
		if (-d "$bad_dir/$d") {
			$d =~ /^.*-(.+)$/g;
			#found a release, increase it's frequency
			my $group = $1;
			#check that the groupname doesn't contain spaces (if it does, it's not a groupname)
			unless ($group =~ / /) {
				#for some reason, this can't be put in the line above
				if (length($group) < 10) {
					$group = uc($group);
					$releases{$group}{'bad'}++;
					$dirs{$good_dir}{'bad'}++;
				}
			}
		}
	}
	closedir BAD;
}

my $total = ($bad_total + $good_total);
#print "GROUP                GOOD   |    BAD   [k:d]-ratio |  % kept\n";
print "GROUP                GOOD   |    BAD   |  % kept\n";
print "---------------------------------------------------------\n";
foreach my $key (sort sorter keys %releases) {
	my $good = $releases{$key}{'good'};
	my $bad = $releases{$key}{'bad'};
	my $gcd = Math::BigInt::bgcd($good, $bad);
	my $kept_percentage = 0;
	if (($good > 1 || $bad > 5) && length($key) > 1 && length($key) < 13) {
		if ($good > 0) {
			$kept_percentage = (100/(($good + $bad) / $good));
		}
		my $good_percent = sprintf( "%.2f", ($good / $good_total) * 100);
		my $bad_percent = sprintf( "%.2f", ($bad / $bad_total) * 100);
		#printf "%-10s = [%3d/%.1f%% | %3d/%.1f%%] [%3d:%-3d] [%.1f%%]\n", $key ,$good, $good_percent, $bad, $bad_percent, ($good / $gcd), ($bad / $gcd), $kept_percentage;	
		printf "%-10s = [%3d/%.1f%% | %3d/%.1f%%] [%.1f%%]\n", $key ,$good, $good_percent, $bad, $bad_percent, $kept_percentage;	
	}	
}

sub sorter {
	if ($sort_by eq "good") {
		{$releases{$b}{'good'} <=> $releases{$a}{'good'}};
	} else {
		if ($sort_by eq "bad") {
			{$releases{$b}{'bad'} <=> $releases{$a}{'bad'}};
		}
	}
}

## use case here for additional features, i.e. don't run all this together unless specified
## ALWAYS DELETED

if ($get_deleted) {
	print "Always deleted:\n";
	foreach my $key (keys %releases) {
		if ($releases{$key}{'bad'} > 0 && $releases{$key}{'good'} == 0) {
			print "$key\n";
		}
	}
	print "Almost always deleted:\n";
	foreach my $key (keys %releases) {
		if ($releases{$key}{'bad'} > 2 && $releases{$key}{'good'} < 4 && $releases{$key}{'good'} != 0) {
			print "$key\n";
		}
	}
}
print "releases per dir\n";
foreach my $key (keys %dirs) {
	print "$key: (good) " . $dirs{$key}{'good'} . "\n";
}