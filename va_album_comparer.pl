#!/usr/bin/perl

# this script will compare the tracklistings of "va"-albums, either from filenames
# or from tracknamed .cue-files, and determine if two or more contain the same, or almost the same tracklistings

use diagnostics;
use strict;
use warnings;


my ($dir) = @ARGV;
my %albums;
my %hit_db;
my $d;
opendir (DIR, $dir) || die "could not open $dir\n";
	while (defined($d = readdir DIR)) {
		#check if the entry is a dir (-d)
		if ($d =~ /^VA|V.A|various.+/i) {
			#is VA-album	
			add_tracks("$dir/$d", \%albums);
		}
	}
closedir DIR;

#print "Albums compared: \n";
#
#foreach my $key (keys %albums) {
#	print "$key \n";
##	foreach my $aa (sort keys %{$albums{$key}}) {
##		#print $$albums{$aa}{'performer'} . "\n";
##		print "\t$aa. " . $albums{$key}->{$aa}{'performer'} . " - " . $albums{$key}->{$aa}{'title'} . "\n";
##	}
#}
#print "\n";

#this only does 2-part comparisons, it's a bit trickier to do comparisons on multiple targets.
#that requires a "hit database", that will store the percentage of a comparison for 2 albums, 
#and them compare every other album to that "hit entry"
foreach my $first (keys %albums) {
	foreach my $second (keys %albums) {
		next if ($first eq $second); #skip this comparison if both loops are looking at the same album
		#print "Comparing: $first TO $second\n";
		#make sure the references work here
		#$hit_db{$first}{$second} = 
		if (compare($albums{$first}, $albums{$second})) {
			print "Albums \n 1. $first \n 2. $second\n";	
			print "---------------------------------------------\n\n";
		} 
	}		
	#put flag here to prevent this album to be compared to albums its already been compared to 
	#takes algorith complexity from O(n^2) -> O((n^2)/2), while not much, it's a bit
	delete $albums{$first};
}

#support: "what albums have this (specified) song?"
sub add_tracks {
	my $dir = shift;
	my $albums = shift;
	my @cues;
	my $files = 0;
	my $f;
	my $return_value;
	opendir (TMP, $dir) || die "could not open $dir in add_tracks()\n";
	while (defined($f = readdir TMP)) {
		#optimize code here, by making the assumption that ++files > say 5 is an album, and loop no further
		if ($f =~ /.+\.cue/) {
			$cues[++$#cues] = "$dir/$f";
			$files = 0;
		} else {
			if (($f =~ /.+\.mp3/) && ($#cues == -1)) {
				$files = 1;
			}
		}
	}
	#cue-files get preference, because if there's a cue, it's the only way to go, but there will ALWYAS be files, obviously
	if ($files && ($#cues == -1)) {
		$return_value = get_tracks_from_dir($dir);
		if (defined($return_value)) {
			$$albums{$dir} = $return_value;	
		}
	} else {
		$return_value = get_tracks_from_cue(@cues);
		if (defined($return_value)) {
			$$albums{$dir} = $return_value;	
		}
	}
	closedir TMP;
}

sub get_tracks_from_cue {
	my $cue_name;
	my %album;
	my $cd_number = 1;
	foreach my $cue_name (@_) {
		my $last_track_number = 0;
		open (CUE, $cue_name) || die "could not open $cue_name\n";
		while (my $line = <CUE>) {
			if ($line =~ /TRACK.*(\d\d).*/) {
				$last_track_number = $1;
			}
			if (($line =~ /TITLE \"(.*)\"/) && $last_track_number != 0) {
				unless (!defined($1) || (lc($1) =~ /track.*/) || (length(lc($1)) == 0)) {
					$album{"$cd_number$last_track_number"}{'title'} = lc($1);	
				} else {
					$album{"$cd_number$last_track_number"}{'title'}	= "unknown";
				}
			}
			if (($line =~ /PERFORMER \"(.*)\"/) && $last_track_number != 0) {
				unless (!defined($1) || (lc($1) =~ /(?:unknown artist|unknown|artist).*/) || (length(lc($1)) == 0)) {
					$album{"$cd_number$last_track_number"}{'performer'}	= lc($1);
				} else {
					$album{"$cd_number$last_track_number"}{'performer'}	= "unknown";
				}
			}
		}	
		close CUE;
		$cd_number++;
	}
	if (%album) {
		return \%album;
	} else {
		return undef;	
	}
}

sub get_tracks_from_dir {
	my $dir_name = shift;
	my %album;
	my $f;
	opendir (FILES, $dir_name) || die "could not open $dir_name\n";
	while (defined($f = readdir FILES)) {
		unless (-d "$dir_name/$f") {
			if ($f =~ /(\d{2,3})(?:-|_)(.+)(?:_-_|-)(.+)-.+\.(.{3})/) {
				my ($trackno, $artist, $title, $extension) = ($1, $2, $3, $4);
				if ($extension eq "mp3") {
					$artist =~ s/(?:_|-)/ /g;
					$title =~ s/(?:_|-)/ /g;
					$album{$trackno}{'title'} = $title;
					$album{$trackno}{'performer'} = $artist;
				}	
			}
		}
	}
	closedir FILES;
	if (%album) {
		return \%album;
	} else {
		return undef;	
	}
}

sub compare {
	#don't check tracks where the artist AND title, usually on the whole album, is unknown
	my $first_ref = shift;
	my $second_ref = shift;
	my $equal_tracks = 0;
	my $tracks = 0;
	my @hit_list;
	my $track_list = "";
	foreach my $i (keys %$first_ref) {
		$tracks = 0;
		#this nullifies a whole album if the first tune is unknown.
		#but if it is, then all the others probably are aswell, if the rippers have done their homework
		
		# NOTE: dirs tagged with -- didn't pass inspection in cue, and needs to be included at a later date
		
		next if ($$first_ref{$i}{'title'} eq "unknown");
		foreach my $j (keys %$second_ref) {
			$tracks++;
			if ((remove_remix($$first_ref{$i}{'title'}) eq remove_remix($$second_ref{$j}{'title'})) && ($$first_ref{$i}{'performer'} eq $$second_ref{$j}{'performer'})) {
				 $equal_tracks++;
				 $track_list .= "MATCH: $$first_ref{$i}{'performer'} - $$first_ref{$i}{'title'}\n";
				 #$hit_list[$#hit_list] = "$$first_ref{$i}{'performer'} - $$first_ref{$i}{'title'}";
			}
		}
	}
	if ($equal_tracks > 1) {
		print $track_list;
		print "$equal_tracks out of ~$tracks (" . (sprintf("%.2f", ($equal_tracks / $tracks) * 100)) . "%)\n";
		#return {'hits' => $equal_tracks, 'total_tracks' => $tracks, 'hit_list' => \@hit_list};
		return 1;
	}
	return 0;
}
sub max {
	my $one = shift;
	my $two = shift;
	if ($one > $two) {
		return $one;	
	} else {
		return $two;
	}
}
sub remove_remix {
	my $track_name = shift;
	$track_name =~ s/(\(.*\))//;
	return $track_name;
}