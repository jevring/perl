#!/bin/perl
@G = ( 
"Blues","Classic Rock","Country",
"Dance","Disco","Funk","Grunge",
"Hip-Hop","Jazz","Metal","New Age",
"Oldies","Other","Pop","R&B","Rap",
"Reggae","Rock","Techno","Industrial",
"Alternative","Ska","Death Metal",
"Pranks","Soundtrack","Euro-Techno",
"Ambient","Trip-Hop","Vocal",
"Jazz+Funk","Fusion","Trance",
"Classical","Instrumental","Acid",
"House","Game","Sound Clip","Gospel",
"Noise","AlternRock","Bass","Soul",
"Punk","Space","Meditative",
"Instrumental Pop","Instrumental Rock",
"Ethnic","Gothic","Darkwave",
"Techno-Industrial","Electronic",
"Pop-Folk","Eurodance","Dream",
"Southern Rock","Comedy","Cult",
"Gangsta","Top 40","Christian Rap",
"Pop/Funk","Jungle","Native American",
"Cabaret","New Wave","Psychadelic",
"Rave","Showtunes","Trailer","Lo-Fi",
"Tribal","Acid Punk","Acid Jazz",
"Polka","Retro","Musical",
"Rock & Roll","Hard Rock","Unknown" );

open(MP3, @ARGV[0]);
seek(MP3, -128, 2); #the id3 1 tag is 128 bytes big
read(MP3, $tag,3);

if($tag eq "TAG")
{
 read(MP3, $title, 30);
 read(MP3, $artist, 30);
 read(MP3, $album, 30);
 read(MP3, $year, 4);
 read(MP3, $comment,29);
 read(MP3, $genre,1); #just getting rid of a garbage char, i think... =)
 read(MP3, $genre,1); #the num representing the genre
 
 for($f = 0;$f < 255; $f++)
 {
  if(sprintf("%c",$f) eq $genre) { $genre = $f; $f = 255; } #find what number the genre $char represents
 }
 
 print "id3v1 tag:\n";
 print " title:   $title\n";
 print " artist:  $artist\n";
 print " album:   $album\n";
 print " year:    $year\n";
 print " genre:   @G[$genre]\n";
 print " comment: $comment\n";
}
