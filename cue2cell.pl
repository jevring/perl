#!/usr/local/bin/perl

use POSIX; #for ceil() and floor()
use Getopt::Long;
use strict;


my $cue_file = '';
my $cell_file = '';
my $fps = ''; 
GetOptions ('fps:s' => \$fps, 'cell:s' => \$cell_file, 'cue:s' => \$cue_file);

if ($fps = '') {
	$fps = 25;	
} else {
	$fps = ceil($fps);	
}
my $fps = ceil(25); #temp assignment
my $divisor = 100 / $fps;
my $minute;
my $second;
my $deci;
open (CUE, $cue_file) || die "couln't open $cue_file\n";
open (CELL, ">$cell_file") || die "couldn't open $cell_file\n";
while(my $line = <CUE>) {
	if ($line =~ /INDEX.+(\d\d):(\d\d):(\d\d)/) {
		($minute, $second, $deci) = ($1, $2, $3);
		my $frames = ceil($deci / $divisor) + ($second * $fps) + ($minute * 60 * $fps);
		unless ($frames == 0) {
			print CELL "$frames\n";
		}
	}
}
close CUE;
close CELL;
